### INK
ink is a C99 single file header library that provides a generic data model implementation. ink is inspired by Our Machinery's Truth data model ( https://ourmachinery.com/post/the-story-behind-the-truth-designing-a-data-model/ ).

To keep the library simple the data model does not natively support pointers but by using subobjects and references all kinds of data structures should be convertable to fit the model.

## using ink
include `ink.h` for the interface declaration and in one of you source files `#define INK_IMPLEMNTATION` before the include to get the definitions. 


## for consideration
# Should renferences and subobjects support specifying a descriptor to limit which objects can be referenced?
C code would already know and probably do the right thing by default. It would be annoying to check changes coming from C code for validity of the data.
Generic editors would benefit from hints. Especially creating new objects would be much easier as subobjects could be created recursively without the need for additional input.
# Should descriptors have names?

# should make_database make a copy of the descriptors?
