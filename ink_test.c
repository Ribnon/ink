#define INK_IMPLEMENTATION
#include "ink.h"

#include <stdio.h>
#include <inttypes.h>
#include <stddef.h>
#include <stdlib.h>

typedef struct vec3
{
	ink_object_id name;
	float x,y,z;
} vec3;

typedef struct Maxink_property_type
{
	ink_object_id refVec3;
	ink_object_id valVec3;
} Maxink_property_type;

typedef struct MetaProperty
{
	ink_object_id name;
	ink_property_type type;
	uint32_t offset;
} MetaProperty;

typedef struct MetaDescriptor
{
	ink_object_id properties;
	uint32_t size;
} MetaDescriptor;


typedef struct MemcpySerializer
{
	void* buffer;
	uint64_t processedBytes;
} MemcpySerializer;

ink_bool serializeMemcpyWrite(ink_u64 size, void const* source_buffer, void* userdata)
{
	MemcpySerializer* writer = (MemcpySerializer*) userdata;
	memcpy((uint8_t*)writer->buffer + writer->processedBytes, source_buffer, size);
	writer->processedBytes += size;
	return true;
}

ink_bool serializeMemcpyRead(ink_u64 size, void* target_buffer, void* userdata)
{
	MemcpySerializer* writer = (MemcpySerializer*) userdata;
	memcpy(target_buffer, (uint8_t*)writer->buffer + writer->processedBytes, size);
	writer->processedBytes += size;
	return true;
}

void* inkAllocator(ink_u64 old_size, void* old_buffer, ink_u64 new_size, void* userdata)
{
	if(0 == old_size || NULL == old_buffer)
	{
		return malloc(new_size);
	}
	else if(0 == new_size)
	{
		free(old_buffer);
		return NULL;
	}
	else
	{
		return realloc(old_buffer, new_size);
	}
}

void print_by_descriptor(ink_database* db, ink_object_id ink_object_id);

void print_property(ink_database* db, ink_property_type type, char const* name, void* data)
{
	ink_enum_id enum_id;
	ink_descriptor_id desc_id;
	if(ink_decode_enum_propety_type(type, &enum_id))
	{
		ink_i32 value = *(ink_i32*)data;
		char const * name = "unkown enum value";
		ink_enum_value_to_name(&db->enums[enum_id], value, &name);
		printf("enum(%" PRIu32 "): %s(%" PRIu32 ")\n", enum_id, name, value);
	}
	else if(ink_decode_reference_type(type, &desc_id))
	{
		char const * typeString = "unkown reference type";
		if(ink_property_subobject == (type & ink_property_subobject))
		{
			typeString = "subobject";
		}
		else if(ink_property_reference == (type & ink_property_reference))
		{
			typeString = "refrence";
		}

		char const * name = "";
		if(db->descriptors[desc_id].name)
		{
			name = db->descriptors[desc_id].name;
		}

		ink_object_id* id = (ink_object_id*)data;
		printf("%s(desc: %s %" PRIu32 ") id: %" PRIu64 "\n", typeString, name, desc_id, *id);
		print_by_descriptor(db, *id);
	}
	else
	{
		switch(type)
		{
			case ink_property_i32:
			{
				int32_t val = *(int32_t*)data;
				printf("\"%s\": %" PRIi32 "\n", name, val);
				break;
			}
			case ink_property_i64:
			{
				int64_t val = *(int64_t*)data;
				printf("\"%s\": %" PRIi64 "\n", name, val);
				break;
			}
			case ink_property_u32:
			{
				uint32_t val = *(uint32_t*)data;
				printf("\"%s\": %" PRIu32 "\n", name, val);
				break;
			}
			case ink_property_u64:
			{
				uint64_t val = *(uint64_t*)data;
				printf("\"%s\": %" PRIu64 "\n", name, val);
				break;
			}
			case ink_property_f32:
			{
				double val = *(float*)data;
				printf("\"%s\": %f\n", name, val);
				break;
			}
			case ink_property_f64:
			{
				float val = *(float*)data;
				printf("\"%s\": %f\n", name, val);
				break;
			}
			//case kString:
			//{
			//	char* string = *(char**)data;
			//	printf("\"%s\": %s\n", name, string);
			//	break;
			//}
			case ink_property_subobject:
			{
				ink_object_id* id = (ink_object_id*)data;
				printf("subobject id: %" PRIu64 "\n", *id);
				print_by_descriptor(db, *id);
				break;
			}
			case ink_property_reference:
			{
				ink_object_id* id = (ink_object_id*)data;
				printf("object reference id: %" PRIu64 "\n", *id);
				print_by_descriptor(db, *id);
				break;
			}
			case ink_property_enum:
			{

			}
		}
	}
}

void print_by_descriptor(ink_database* db, ink_object_id ink_object_id)
{
	ink_object const * obj = ink_get_object_by_id(db, ink_object_id);
	ink_object_descriptor const * descriptor = ink_get_descriptor_by_id(db, obj->descriptor_id);

	bool isString = descriptor->property_count == 1 && descriptor->property_types[0] == ink_property_u32;
	if(isString)
	{
		printf("string: \"");
		uint32_t const arrayLength = obj->element_count;
		uint32_t arrayIndex = 0;
		do
		{
			printf("%c", *(unsigned char*)((uint8_t*)obj->data +descriptor->size * arrayIndex + descriptor->property_offsets[0]));
			++arrayIndex;
		} while( arrayIndex < arrayLength );
		printf("\"\n");
	}
	else
	{
		uint32_t const arrayLength = obj->element_count;
		uint32_t arrayIndex = 0;
		do
		{
			for(uint32_t propertyIndex = 0; propertyIndex < descriptor->property_count; ++propertyIndex)
			{
				print_property(db, descriptor->property_types[propertyIndex], descriptor->property_names[propertyIndex], (uint8_t*)obj->data +descriptor->size * arrayIndex + descriptor->property_offsets[propertyIndex]);
			}
			++arrayIndex;
		} while( arrayIndex < arrayLength );
	}
}

int main()
{
	printf("main start\n");

	char const * const ink_type_enum_names[] = {"subobject", "reference", "enum", "i32", "i64", "u32", "u64", "f32", "f64"};
	ink_i32 ink_type_enum_values[] = {ink_property_subobject, ink_property_reference, ink_property_enum, ink_property_i32, ink_property_i64, ink_property_u32, ink_property_u64, ink_property_f32, ink_property_f64};
	ink_enum my_enum;
	my_enum.names = ink_type_enum_names;
	my_enum.values = ink_type_enum_values;
	my_enum.size = 9;

	char const * const names[] = {"name", "x", "y", "z"};
	ink_property_type const types[] = {ink_encode_reference_type(ink_property_subobject, 5), ink_property_f32, ink_property_f32, ink_property_f32};
	uint32_t const offsets[] = {0, 8, 12, 16};

	ink_object_descriptor vec3Desc;
	vec3Desc.name ="vec3";
	vec3Desc.property_count = 4;
	vec3Desc.property_names = names;
	vec3Desc.property_types = types;
	vec3Desc.property_offsets = offsets;
	vec3Desc.size = sizeof(vec3);

	char const * const max_names[] = {"ref", "val"};
	ink_property_type const max_types[] = {ink_encode_reference_type(ink_property_reference, 0), ink_encode_reference_type(ink_property_subobject, 0)};
	uint32_t const max_offsets[] = {0, 8};

	ink_object_descriptor maxDesc;
	{
		maxDesc.name = "max";
		maxDesc.property_count = 2;
		maxDesc.property_names = max_names;
		maxDesc.property_types = max_types;
		maxDesc.property_offsets = max_offsets;
		maxDesc.size = sizeof(Maxink_property_type);
	}

	uint32_t name_string[] = {'n', 'a', 'm', 'e'};
	uint32_t type_string[] = {'t', 'y', 'p', 'e'};
	uint32_t offset_string[] = {'o', 'f', 'f', 's', 'e', 't'};

	char const *  const metaProp_names[] = {"name", "type", "offset"};
	ink_property_type const metaProp_types[] = {ink_encode_reference_type(ink_property_subobject, 5), ink_encode_enum_property_type(0), ink_property_u32};
	uint32_t const metaProp_offsets[] = {offsetof(MetaProperty, name), offsetof(MetaProperty, type), offsetof(MetaProperty, offset)};

	ink_object_descriptor metaPropertyDesc;
	{
		metaPropertyDesc.name = "meta property";
		metaPropertyDesc.property_count = 3;
		metaPropertyDesc.property_names = metaProp_names;
		metaPropertyDesc.property_types = metaProp_types;
		metaPropertyDesc.property_offsets = metaProp_offsets;
		metaPropertyDesc.size = sizeof(MetaProperty);
	}

	char const * const meta_names[] = {"properties", "size"};
	ink_property_type const meta_types[] = {ink_encode_reference_type(ink_property_subobject, 4), ink_property_u32};
	uint32_t const meta_offsets[] = {offsetof(MetaDescriptor, properties), offsetof(MetaDescriptor, size)};

	ink_object_descriptor metaDescriptor;
	{
		metaDescriptor.name = "meta descriptor";
		metaDescriptor.property_count = 2;
		metaDescriptor.property_names = meta_names;
		metaDescriptor.property_types = meta_types;
		metaDescriptor.property_offsets = meta_offsets;
		metaDescriptor.size = sizeof(metaDescriptor);
	}

	char const * const vec3Array_names[] = { "vec3s" };
	ink_property_type const vec3Array_types[] = { ink_encode_reference_type(ink_property_reference, 0) };
	uint32_t const vec3Array_offsets[] = { 0 };

	ink_object_descriptor vec3ArrayDesc;
	{
		vec3ArrayDesc.name = "vec3Array";
		vec3ArrayDesc.property_count = 1;
		vec3ArrayDesc.property_names = vec3Array_names;
		vec3ArrayDesc.property_types = vec3Array_types;
		vec3ArrayDesc.property_offsets = vec3Array_offsets;
		vec3ArrayDesc.size = sizeof(ink_object_id);
	}

	char const * const string_names[] = {"chars"};
	ink_property_type const string_types[] = {ink_property_u32};
	uint32_t const string_offsets[] = {0};

	ink_object_descriptor stringDesc;
	{
		stringDesc.name = "string";
		stringDesc.property_count = 1;
		stringDesc.property_names = string_names;
		stringDesc.property_types = string_types;
		stringDesc.property_offsets = string_offsets;
		stringDesc.size = sizeof(uint32_t);
	}

	ink_object_descriptor descs[] = {vec3Desc, maxDesc, metaDescriptor, vec3ArrayDesc, metaPropertyDesc, stringDesc};

	ink_database* db = ink_make_database(inkAllocator, NULL, 6, descs, 1, &my_enum);

	uint32_t doof_name[] = {'d', 'o', 'o', 'f'};
	uint32_t max_name[] = {'m', 'a', 'x'};

	ink_object_id doofName = ink_make_object(db, 5, 4, doof_name);
	ink_object_id maxName = ink_make_object(db, 5, 3, max_name);

	vec3 doof;
	doof.name = doofName;
	doof.x = 12;
	doof.y = 5;
	doof.z = 3;

	vec3 max_vec3;
	max_vec3.name = maxName;
	max_vec3.x = 7;
	max_vec3.y = 8;
	max_vec3.z = 9;

	ink_object_id doofId = ink_make_object(db, 0, 1, &doof);
	ink_object_id max_vec3Id = ink_make_object(db, 0, 1, &max_vec3);

	ink_object_id metaNameString = ink_make_object(db, 5, 4, name_string);
	ink_object_id metaink_property_typeString = ink_make_object(db, 5, 4, type_string);
	ink_object_id metaOffsetString = ink_make_object(db, 5, 6, offset_string);

	MetaProperty metaProperties[] = {
		{ metaNameString, ink_property_subobject, offsetof(MetaProperty, name) },
		{ metaink_property_typeString, ink_property_enum, offsetof(MetaProperty, type) },
		{ metaOffsetString, ink_property_u32, offsetof(MetaProperty, offset) }
	};

	ink_object_id metaPropId = ink_make_object(db, 4, 3, &metaProperties);

	Maxink_property_type max;
	max.refVec3 = doofId;
	max.valVec3 = max_vec3Id;

	MetaDescriptor meta;
	{
		meta.properties = metaPropId;
		meta.size = sizeof(vec3);
	}

	ink_object_id maxId = ink_make_object(db, 1, 1, &max);
	ink_object_id metaId = ink_make_object(db, 2, 1, &meta);

	doof.name = doofName;
	max_vec3.name = maxName;
	ink_object_id v3arrayIds[] = {doofId, max_vec3Id};

	ink_object_id v3arrayId = ink_make_object(db, 3, 2, v3arrayIds);

	printf("doof: id %" PRIu64 ":\n", doofId);
	print_by_descriptor(db, doofId);
	printf("max id %" PRIu64 ":\n", maxId);
	print_by_descriptor(db, maxId);
	printf("meta id %" PRIu64 ":\n", metaId);
	print_by_descriptor(db, metaId);
	printf("v3array id %" PRIu64 ":\n", v3arrayId);
	print_by_descriptor(db, v3arrayId);


	ink_object_id updateId = doofId;
	uint32_t updatedelement_count = 1;
	vec3 doofUpdate;
	ink_begin_object_change(db, updateId, &doofUpdate);
	doofUpdate.name = doofName;
	doofUpdate.x = 13;
	doofUpdate.y = 4;
	doofUpdate.z = 314;

	void* updatedData = &doofUpdate;
	ink_commit_changes(db, 1, &updateId, &updatedelement_count, &updatedData);

	printf("updated id %" PRIu64 ":\n", doofId);
	print_by_descriptor(db, doofId);

	ink_undo_previous_action(db);
	printf("undid id %" PRIu64 ":\n", doofId);
	print_by_descriptor(db, doofId);

	uint64_t bufferSize = 1024 * 1024;
	void* serializationBuffer = malloc(bufferSize);

	MemcpySerializer serializer;
	serializer.buffer = serializationBuffer;
	serializer.processedBytes = 0;

	ink_serialize_database_to_buffer(db, serializeMemcpyWrite, &serializer);
	ink_u64 writtenBytes = serializer.processedBytes;
	printf("serialized db in %" PRIu64 " bytes\n", writtenBytes);

	FILE* dbFile = fopen("datamodel.db", "wb");
	fwrite(serializationBuffer, writtenBytes, 1, dbFile);
	fclose(dbFile);

	dbFile = fopen("datamodel.db", "rb");
	fread(serializationBuffer, writtenBytes, 1, dbFile);
	fclose(dbFile);

	ink_database* db2 = ink_make_database(inkAllocator, NULL, 6, descs, 1, &my_enum);

	serializer.processedBytes = 0;
	bool loadSuccess = ink_serialize_database_from_buffer(db2, serializeMemcpyRead, &serializer);
	printf(loadSuccess ? "db load succeeded\n" : "db load failed\n");
	if(false == loadSuccess)
	{
		return 1;
	}

	printf("id 0:\n");
	print_by_descriptor(db2, doofId);
	printf("id 1:\n");
	print_by_descriptor(db2, maxId);
	printf("id 3:\n");
	print_by_descriptor(db2, metaId);
	printf("id 4:\n");
	print_by_descriptor(db2, v3arrayId);


	return 0;
}
