#ifndef INK_H
#define INK_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

typedef bool ink_bool;
typedef int32_t ink_i32;
typedef uint32_t ink_u32;
typedef uint64_t ink_u64;
typedef float ink_f32;
typedef double ink_f64;

typedef ink_u64 ink_object_id;
typedef ink_u32 ink_descriptor_id;
typedef ink_u64 ink_undo_id;
typedef ink_u32 ink_enum_id;

typedef enum ink_property_type
{
	ink_property_i32 = 1 << 2,
	ink_property_i64 = 2 << 2,
	ink_property_u32 = 3 << 2,
	ink_property_u64 = 4 << 2,
	ink_property_f32 = 5 << 2,
	ink_property_f64 = 6 << 2,
	//kString,
	ink_property_subobject = 1,
	ink_property_reference = 2,
	ink_property_enum = 3
} ink_property_type;

typedef struct ink_database ink_database;
typedef struct ink_object_descriptor ink_object_descriptor;
typedef struct ink_enum ink_enum;
typedef struct ink_object ink_object;

typedef void* (*ink_PFN_reallocate)(ink_u64 old_size, void* old_buffer, ink_u64 new_size, void* userdata);
typedef ink_bool (*ink_PFN_serialize_write)(ink_u64 size, void const* source_buffer, void* userdata);
typedef ink_bool (*ink_PFN_serialize_read)(ink_u64 size, void* target_buffer, void* userdata);

// create a new database
ink_database* ink_make_database(ink_PFN_reallocate allocator, void* allocator_userdata, ink_u32 descriptor_count, ink_object_descriptor const* descriptors, ink_u32 enum_count, ink_enum const* enums);

// stores all objects and properties in `buffer` if `buffer_size` is large enough.
// returns number of bytes written to `buffer`
// the serialized data can be loaded into a database with matching descriptors by calling `ink_serialize_database_from_buffer`
ink_bool ink_serialize_database_to_buffer(ink_database* db, ink_PFN_serialize_write write_fn, void* write_fn_userdata);

// loads objects and properties from `buffer` that was produced by `ink_serialize_database_to_buffer`
// the descriptor_ids have to match between both databases for this to work.
// `db` is assumed to contain 0 objects.
// returns `true` if all objects and properties loaded succesfully.
bool ink_serialize_database_from_buffer(ink_database* db, ink_PFN_serialize_read read_fn, void* read_fn_userdata);

// create a new object in `db`
// if `data != NULL` the object is initilaized with `data`. The caller must guarantee enough bytes are readable to statisfy the descriptor.
ink_object_id ink_make_object(ink_database* db, ink_descriptor_id descriptor_id, ink_u32 element_count, void const * data);

ink_object_descriptor const* ink_get_descriptor_by_id(ink_database const * db, ink_descriptor_id descriptor_id);
ink_object const* ink_get_object_by_id(ink_database const * db, ink_object_id object_id);

//
void ink_begin_object_change(ink_database const * db, ink_object_id ink_object_id, void* out_data);
ink_undo_id ink_commit_changes(ink_database* db, ink_u32 change_count, ink_object_id const* object_ids, ink_u32 const * updated_element_counts, void ** changed_objects);
void ink_undo_previous_action(ink_database* db);

ink_bool ink_enum_get_all_names(ink_enum const* ink_enum, ink_i32* out_count, char const* const ** out_names);
ink_bool ink_enum_value_to_name(ink_enum const* ink_enum, ink_i32 value, char const ** out_name);
ink_bool ink_enum_name_to_value(ink_enum const* ink_enum, char const* name, ink_i32* out_value);

char const * ink_property_type_to_string(ink_property_type type);
ink_u32 ink_type_to_alignment(ink_property_type type);

ink_property_type ink_encode_enum_property_type(ink_enum_id enum_id);
ink_bool ink_decode_enum_propety_type(ink_property_type property_type, ink_enum_id* out_enum_id);

ink_property_type ink_encode_reference_type(ink_property_type sub_or_ref, ink_descriptor_id descriptor_id);
ink_bool ink_decode_reference_type(ink_property_type type, ink_descriptor_id* out_descriptor_id);

uint64_t ink_get_database_size(ink_database const* ink_db);
ink_enum const * ink_get_enum(ink_database const* ink_db, uint32_t enum_index);

struct ink_object_descriptor
{
	char const * name;
	char const ** property_names;
	ink_property_type const * property_types;
	ink_u32 const * property_offsets;
	ink_u32 size;
	ink_u32 property_count;
};

struct ink_enum
{
	char const * const * names;
	ink_i32 const * values;
	ink_u32 size;
};

struct ink_object
{
	ink_descriptor_id descriptor_id;
	ink_u32 element_count;
	void* data;
};

//#define INK_IMPLEMENTATION // for dev purposes only

#ifdef INK_IMPLEMENTATION

#include <string.h>


char const * ink_property_type_to_string(ink_property_type const type)
{
	switch(type)
	{
		case ink_property_i32: return "i32";
		case ink_property_i64: return "i64";
		case ink_property_u32: return "u32";
		case ink_property_u64: return "u64";
		case ink_property_f32: return "f32";
		case ink_property_f64: return "f64";
		//case kString: return "string";
		case ink_property_subobject: return "subobject";
		case ink_property_reference: return "reference";
		case ink_property_enum : return "enum"; //
		default: return "undefined type";
	}
}

uint32_t ink_type_to_alignment(ink_property_type const type)
{
	switch(type)
	{
		case ink_property_i32: return sizeof(int32_t);
		case ink_property_i64: return sizeof(int64_t);
		case ink_property_u32: return sizeof(uint32_t);
		case ink_property_u64: return sizeof(uint64_t);
		case ink_property_f32: return sizeof(float);
		case ink_property_f64: return sizeof(double);
		//case kString: return sizeof(char *);
		case ink_property_subobject: return sizeof(ink_object_id);
		case ink_property_reference: return sizeof(ink_object_id);
		case ink_property_enum: return sizeof(ink_i32);
		default: return 0;
	}
}

ink_property_type ink_encode_enum_property_type(ink_enum_id enum_id)
{
	return (ink_property_type)(ink_property_enum | (enum_id << 2));
}

ink_bool ink_decode_enum_propety_type(ink_property_type property_type, ink_enum_id* out_id)
{
	if(ink_property_enum != (property_type & ink_property_enum))
	{
		return false;
	}
	*out_id = (property_type >> 2);
	return true;;
}

ink_property_type ink_encode_reference_type(ink_property_type sub_or_ref, ink_descriptor_id descriptor_id)
{
	return (ink_property_type)(sub_or_ref | (descriptor_id << 2));
}

ink_bool ink_decode_reference_type(ink_property_type type, ink_descriptor_id* out_id)
{
	if(ink_property_subobject == (type & ink_property_subobject)
	|| ink_property_reference == (type & ink_property_reference))
	{
		*out_id = (type >> 2);
		return true;
	}
	return false;
}

ink_bool ink_enum_get_all_names(ink_enum const* ink_enum, ink_i32* out_count, char const* const ** out_names)
{
	if(NULL == out_count || NULL == out_names)
	{
		return false;
	}

	*out_count = ink_enum->size;
	*out_names = ink_enum->names;
	return true;
}

ink_bool ink_enum_value_to_name(ink_enum const* ink_enum, ink_i32 value, char const ** out_name)
{
	for(ink_u32 entryIndex = 0; entryIndex < ink_enum->size; ++entryIndex)
	{
		if(ink_enum->values[entryIndex] == value)
		{
			*out_name = ink_enum->names[entryIndex];
			return true;
		}
	}
	return false;
}

ink_bool ink_enum_name_to_value(ink_enum const* ink_enum, char const* name, ink_i32* out_value)
{
	for(ink_u32 entryIndex = 0; entryIndex < ink_enum->size; ++entryIndex)
	{
		if(0 == strcmp(ink_enum->names[entryIndex], name))
		{
			*out_value = ink_enum->values[entryIndex];
			return true;
		}
	}
	return false;
}

typedef struct ink_change
{
	ink_object_id object;
	void* old_data;
	void* new_data;
	uint32_t old_element_count;
	uint32_t new_element_count;
} ink_change;

typedef struct ink_change_set
{
	uint32_t first_change;
	uint32_t change_count;
} ink_change_set;

struct ink_database
{
	ink_PFN_reallocate allocator;
	void* allocator_userdata;

	ink_object_descriptor const * descriptors;
	ink_enum const * enums;

	ink_object* object_buffer;
	ink_change* change_buffer;
	ink_change_set* history_buffer;

	ink_u32 descriptor_count;
	ink_u32 enum_count;
	ink_u32 undo_counter;
};

// stb stretchy buffer
// modified to use custom allocator

#define stb_sb_free(a)         ((a) ? free(stb__sbraw(a)),0 : 0)
#define stb_sb_push(db, a,v)	(stb__sbmaybegrow(db, a,1), (a)[stb__sbn(a)++] = (v))
#define stb_sb_count(a)        ((a) ? stb__sbn(a) : 0)
#define stb_sb_add(db, a,n)		(stb__sbmaybegrow(db, a,n), stb__sbn(a)+=(n), &(a)[stb__sbn(a)-(n)])
#define stb_sb_last(a)         ((a)[stb__sbn(a)-1])

#define stb__sbraw(a) ((int *) (void *) (a) - 2)
#define stb__sbm(a)   stb__sbraw(a)[0]
#define stb__sbn(a)   stb__sbraw(a)[1]

#define stb__sbneedgrow(a,n)  ((a)==0 || stb__sbn(a)+(n) >= stb__sbm(a))
#define stb__sbmaybegrow(db, a,n) (stb__sbneedgrow(a,(n)) ? stb__sbgrow(db, a,n) : 0)
#define stb__sbgrow(db, a,n)      (*((void **)&(a)) = stb__sbgrowf((db), (a), (n), sizeof(*(a))))

static void * stb__sbgrowf(ink_database* db, void *arr, int increment, int itemsize)
{
   int dbl_cur = arr ? 2*stb__sbm(arr) : 0;
   int min_needed = stb_sb_count(arr) + increment;
   int m = dbl_cur > min_needed ? dbl_cur : min_needed;
   //int *p = (int *) realloc(arr ? stb__sbraw(arr) : 0, itemsize * m + sizeof(int)*2);
   int *p = (int *) db->allocator(arr ? stb__sbm(arr) : 0, arr ? stb__sbraw(arr) : 0, itemsize * m + sizeof(int)*2, db->allocator_userdata);

   if (p) {
	  if (!arr)
		 p[1] = 0;
	  p[0] = m;
	  return p+2;
   } else {
	  #ifdef STRETCHY_BUFFER_OUT_OF_MEMORY
	  STRETCHY_BUFFER_OUT_OF_MEMORY ;
	  #endif
	  return (void *) (2*sizeof(int)); // try to force a NULL pointer exception later
   }
}

//end of stb stretchy buffer


ink_database* ink_make_database(ink_PFN_reallocate allocator, void* allocator_userdata, ink_u32 descriptor_count, ink_object_descriptor const* descriptors, ink_u32 enum_count, ink_enum const* enums)
{
	ink_database* db = (ink_database*)allocator(0, NULL, sizeof(ink_database), allocator_userdata);

	db->allocator = allocator;
	db->allocator_userdata = allocator_userdata;

	db->descriptor_count = descriptor_count;

	ink_object_descriptor* tempDescs = (ink_object_descriptor *)db->allocator(0, NULL, sizeof(ink_object_descriptor) * db->descriptor_count, db->allocator_userdata);
	db->enum_count = enum_count;
	db->enums = enums;

	db->object_buffer = NULL;
	db->change_buffer = NULL;
	db->history_buffer = NULL;

	memcpy(tempDescs, descriptors, sizeof(ink_object_descriptor) * db->descriptor_count);
	for(uint32_t descriptorIndex = 0; descriptorIndex < db->descriptor_count; ++descriptorIndex)
	{
		tempDescs[descriptorIndex].name = strdup(descriptors[descriptorIndex].name);
		for(uint32_t propertyIndex = 0; propertyIndex < descriptors[descriptorIndex].property_count; ++propertyIndex)
		{
			tempDescs[descriptorIndex].property_names[propertyIndex] = strdup(descriptors[descriptorIndex].property_names[propertyIndex]);
		}
	}

	db->descriptors = tempDescs;
	return db;
}

uint64_t ink_get_database_size(ink_database const * const ink_db)
{
	return stb_sb_count(ink_db->object_buffer);
}

ink_enum const * ink_get_enum(ink_database const* ink_db, uint32_t enum_index)
{
	return &ink_db->enums[enum_index];
}

ink_object_descriptor const* ink_get_descriptor_by_id(ink_database const * db, ink_descriptor_id descriptor_id)
{
	return &db->descriptors[descriptor_id];
}

ink_object const* ink_get_object_by_id(ink_database const * db, ink_object_id object_id)
{
	return &db->object_buffer[object_id];
}

ink_object_id ink_make_object(ink_database* db, ink_descriptor_id descriptor_id, uint32_t element_count, void const * data)
{
	ink_object_descriptor const* desc = ink_get_descriptor_by_id(db, descriptor_id);
	uint32_t buffer_size = desc->size * element_count;
	void* data_buffer = db->allocator(0, NULL, buffer_size, db->allocator_userdata);
	if(NULL != data)
	{
		memcpy(data_buffer, data, buffer_size);
	}
	else
	{
		memset(data_buffer, 0, buffer_size);
	}

	ink_object_id id = stb_sb_count(db->object_buffer);
	ink_object obj;
	obj.data = data_buffer;
	obj.descriptor_id = descriptor_id;
	obj.element_count = element_count;

	stb_sb_push(db, db->object_buffer, obj);

	return id;
}

void ink_begin_object_change(ink_database const * const db, ink_object_id const ink_object_id, void* const outData)
{
	ink_object const* obj = ink_get_object_by_id(db, ink_object_id);
	ink_object_descriptor const* desc = ink_get_descriptor_by_id(db, obj->descriptor_id);
	memcpy(outData, obj->data, desc->size * obj->element_count);
}

ink_undo_id ink_commit_changes(ink_database* const db, uint32_t const change_count, ink_object_id const* const object_ids, uint32_t const * const updated_element_counts, void ** const changed_objects)
{
	if(db->undo_counter != 0)
	{
		// implement partial history reset
	}

	uint32_t firstChange = stb_sb_count(db->change_buffer);

	for(uint32_t change_index = 0; change_index < change_count; ++change_index)
	{
		ink_object_id objId = object_ids[change_index];
		ink_object* obj = &db->object_buffer[objId];

		void * const newData = changed_objects[change_index];
		uint32_t new_element_count = updated_element_counts[change_index];

		ink_change change = {objId, obj->data, newData, obj->element_count, new_element_count};
		stb_sb_push(db, db->change_buffer, change);
		
		{
			// this is supposed to be atomic
			obj->data = newData;
			obj->element_count = new_element_count;
		}
	}

	ink_change_set changes;
	changes.first_change = firstChange;
	changes.change_count = change_count;

	ink_undo_id ink_undo_id = stb_sb_count(db->history_buffer);

	stb_sb_push(db, db->history_buffer, changes);

	return ink_undo_id;
}

void ink_undo_previous_action(ink_database* db)
{
	if(stb_sb_count(db->history_buffer) > (int)db->undo_counter)
	{
		ink_change_set const* changes = &db->history_buffer[db->undo_counter];

		for(uint32_t changeIndex = 0; changeIndex < changes->change_count; ++changeIndex)
		{
			ink_change const* change = &db->change_buffer[changes->first_change + changeIndex];
			//assert(change->newData == db->objects[change->object].data);

			db->object_buffer[change->object].data = change->old_data;
			db->object_buffer[change->object].element_count = change->old_element_count;
		}

		++db->undo_counter;
	}
}

ink_bool ink_serialize_database_to_buffer(ink_database* db, ink_PFN_serialize_write write_fn, void* write_fn_userdata)
{
	// let's assume for now that descriptor ids are handled by the user

	ink_u64 const object_count = stb_sb_count(db->object_buffer);
	ink_u64 const object_size = object_count * sizeof(ink_object);

	ink_bool serialize_ok = true;
	serialize_ok = write_fn(sizeof(ink_u64), &object_count, write_fn_userdata);
	if(false == serialize_ok)
	{
		return false;
	}

	serialize_ok = write_fn(object_size, db->object_buffer, write_fn_userdata);
	if(false == serialize_ok)
	{
		return false;
	}

	for(uint64_t object_index = 0; object_index < object_count; ++object_index)
	{
		ink_object const* obj = ink_get_object_by_id(db, object_index);
		ink_object_descriptor const* desc = ink_get_descriptor_by_id(db, obj->descriptor_id);

		serialize_ok = write_fn(desc->size * obj->element_count, obj->data, write_fn_userdata);
		if(false == serialize_ok)
		{
			return false;
		}

	}

	return true;
}

bool ink_serialize_database_from_buffer(ink_database* db, ink_PFN_serialize_read read_fn, void* read_fn_userdata)
{
	//PRECONDITION:
	// all descriptors referenced by the buffer are already registered in the database with the same indices they had during serialization to the buffer.
	ink_bool serialize_ok = true;

	ink_u64 objectCount;
	serialize_ok = read_fn(sizeof(ink_u64), &objectCount, read_fn_userdata);
	if(false == serialize_ok)
	{
		return false;
	}

	ink_u64 objectSize = objectCount * sizeof(ink_object);


	stb_sb_add(db, db->object_buffer, (int)objectCount);
	serialize_ok = read_fn(objectSize, db->object_buffer, read_fn_userdata);
	if(false == serialize_ok)
	{
		return false;
	}

	ink_u64 dataSize = 0;
	for(ink_u64 objectIndex = 0; objectIndex < objectCount; ++objectIndex)
	{
		ink_object const* obj = ink_get_object_by_id(db, objectIndex);
		ink_object_descriptor const* desc = ink_get_descriptor_by_id(db, obj->descriptor_id);

		dataSize += desc->size * obj->element_count;
	}


	void* dataBuffer = db->allocator(0, NULL, dataSize, db->allocator_userdata);
	serialize_ok = read_fn(dataSize, dataBuffer, read_fn_userdata);
	if(false == serialize_ok)
	{
		return false;
	}

	ink_u64 dataOffset = 0;
	for(ink_u64 objectIndex = 0; objectIndex < objectCount; ++objectIndex)
	{
		ink_object const* obj = ink_get_object_by_id(db, objectIndex);
		ink_object_descriptor const* desc = ink_get_descriptor_by_id(db, obj->descriptor_id);

		db->object_buffer[objectIndex].data = (uint8_t*)dataBuffer + dataOffset;
		dataOffset += desc->size * obj->element_count;
	}

	return true;
}


#endif // INK_IMPLEMENTATION

#ifdef __cplusplus
}
#endif


#endif // INK_H
